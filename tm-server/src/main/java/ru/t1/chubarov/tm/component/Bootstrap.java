package ru.t1.chubarov.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.api.endpoint.*;
import ru.t1.chubarov.tm.api.service.*;
import ru.t1.chubarov.tm.api.service.dto.*;
import ru.t1.chubarov.tm.api.service.model.IProjectService;
import ru.t1.chubarov.tm.api.service.model.IProjectTaskService;
import ru.t1.chubarov.tm.api.service.model.ITaskService;
import ru.t1.chubarov.tm.api.service.model.IUserService;
import ru.t1.chubarov.tm.endpoint.*;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.dto.model.ProjectDTO;
import ru.t1.chubarov.tm.dto.model.UserDTO;
import ru.t1.chubarov.tm.service.*;
import ru.t1.chubarov.tm.service.model.ProjectService;
import ru.t1.chubarov.tm.service.model.ProjectTaskService;
import ru.t1.chubarov.tm.service.model.TaskService;
import ru.t1.chubarov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Setter
@Component
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.chubarov.tm.command";

    @NotNull
    public static final String SERVER_HOST = "server.host";

    @NotNull
    public static final String SERVER_PORT = "server.port";

    @NotNull
    public ApplicationContext context;

    @NotNull
    @Autowired
    private IProjectDtoService projectDtoService;

    @NotNull
    private ICommandService commandService = new CommandService();

    @NotNull
    @Autowired
    private ITaskDtoService taskDtoService;

    @NotNull
    @Autowired
    private IProjectTaskDtoService projectTaskDtoService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    private IPropertyService propertyService = new PropertyService();

    @NotNull
    private ISessionDtoService sessionService;

    @NotNull
    @Autowired
    private IUserDtoService userDtoService;

    @NotNull
    @Autowired
    private IAuthService authService;

    @NotNull
    @Autowired
    private IDomainService domainService;

    @Nullable
    @Autowired
    private IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @Nullable
    @Autowired
    private  IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @Nullable
    @Autowired
    private ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @Nullable
    @Autowired
    private  IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    @Autowired
    private  IProjectService projectService = new ProjectService();

    @NotNull
    @Autowired
    private  ITaskService taskService = new TaskService();

    @NotNull
    private  IProjectTaskService projectTaskService = new ProjectTaskService(projectService, taskService);

    @NotNull
    @Autowired
    private  IUserService userService;

    @NotNull
    private final Backup backup = new Backup(this);

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() throws Exception {

        @NotNull final UserDTO test = userDtoService.create("test", "test", "test@emal.ru");
        @NotNull final UserDTO user = userDtoService.create("user", "user", "user@emal.ru");
        @NotNull final UserDTO admin = userDtoService.create("admin", "admin", Role.ADMIN.toString());

        projectDtoService.add(test.getId(), new ProjectDTO("TEST PROJECT", Status.IN_PROGRESS.toString()));
        projectDtoService.add(user.getId(), new ProjectDTO("ALFA PROJECT", Status.NOT_STARTED.toString()));
        projectDtoService.add(user.getId(), new ProjectDTO("BETA PROJECT", Status.IN_PROGRESS.toString()));
        projectDtoService.add(user.getId(), new ProjectDTO("DELTA PROJECT", Status.COMPLETED.toString()));
        projectDtoService.create(admin.getId(), "DELTA ADMIN PROJECT", "PRJ ADMIN DESC");
        projectDtoService.create(test.getId(), "BETA TEST PROJECT", "PRJ TEST DESC");
        taskDtoService.create(test.getId(), "FIRST TASK", "FIRST DESCRIPTION");
        taskDtoService.create(test.getId(), "SECOND TASK", "SECOND DESCRIPTION");
    }

    {
        regestry(userEndpoint);
        regestry(domainEndpoint);
        regestry(taskEndpoint);
        regestry(projectEndpoint);
        regestry(systemEndpoint);
        regestry(authEndpoint);
    }

    public void regestry(@NotNull final Object endpoint) {
        @NotNull final String host = "0.0.0.0";
        @NotNull final String port = "6060";
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void start() throws Exception {
        initPID();
        initDemoData();
        loggerService.initJmsLogger();
        loggerService.info("** WELCOME TO TASK SERVER MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                prepareShutdown();
            }
        });
    }

    private void prepareShutdown() {
        loggerService.info("** TASK SERVER MANAGER IS SHUTTING DOWN **");
    }

}