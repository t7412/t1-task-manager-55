package ru.t1.chubarov.tm.service;

import lombok.NoArgsConstructor;
import org.apache.activemq.console.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.chubarov.tm.api.repository.ICommandRepository;
import ru.t1.chubarov.tm.api.service.ICommandService;

import java.util.Collection;

@NoArgsConstructor
public final class CommandService implements ICommandService {

    @NotNull
    private ICommandRepository commandRepository;

    @Override
    public void add(@Nullable AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArgument(@Nullable String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return commandRepository.getCommandByArgument(argument);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@Nullable String name) {
        if (name == null || name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @Override
    @NotNull
    public Iterable<AbstractCommand> getCommandWithArgument() {
        return commandRepository.getCommandWithArgument();
    }

}
