package ru.t1.chubarov.tm.api.service;

import org.apache.activemq.console.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.ICommandRepository;

import java.util.Collection;

public interface ICommandService extends ICommandRepository {

    @Nullable
    Collection<AbstractCommand> getTerminalCommands();

    @NotNull
    Iterable<AbstractCommand> getCommandWithArgument();

}
