package ru.t1.chubarov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IDtoRepository<M extends AbstractModelDTO> {

    void add(@Nullable M model);

    void clear();

    @Nullable
    List<M> findAll();

    @Nullable
    M findOneById(@NotNull  String id);

    void remove(@NotNull M model);

    int getSize();

    void update(@Nullable M model);

    boolean existsById(@NotNull String id);

    @NotNull
    EntityManager getEntityManager();

}
