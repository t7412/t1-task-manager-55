package ru.t1.chubarov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.List;

public interface IModelRepository<M extends AbstractModel> {

    void add(@Nullable M model);

    void clear();

    @Nullable
    List<M> findAll();

    @Nullable
    M findOneById(@NotNull  String id);

    void remove(@NotNull M model);

    long getSize();

    void update(@Nullable M model);

    EntityManager getEntityManager();

}
