package ru.t1.chubarov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.dto.model.TaskDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskDtoService {

    @NotNull
    TaskDTO create(@NotNull String userId, @NotNull String name, @NotNull String description) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    List<TaskDTO> findAll(@NotNull String userId) throws Exception;

    @NotNull
    List<TaskDTO> findAll() throws Exception;

    @NotNull
    TaskDTO findOneById(@NotNull String userId, @Nullable String id) throws Exception;

    @Nullable
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId) throws Exception;

    @NotNull
    TaskDTO add(@Nullable String userId, @Nullable TaskDTO model) throws Exception;

    @NotNull
    TaskDTO updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description) throws Exception;

    @NotNull
    TaskDTO changeTaskStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) throws Exception;

    @NotNull
    TaskDTO remove(@NotNull String userId, @Nullable TaskDTO model) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @NotNull
    TaskDTO removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    int getSize() throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    @NotNull
    Collection<TaskDTO> add(@NotNull Collection<TaskDTO> models) throws Exception;

    @NotNull
    Collection<TaskDTO> set(@NotNull Collection<TaskDTO> models) throws Exception;

    void clear();

    @NotNull
    TaskDTO changeTaskStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status) throws Exception;

    @NotNull
    TaskDTO updateByIndex(@NotNull String userId, @NotNull Integer index, @NotNull String name, @NotNull String description) throws Exception;

    @NotNull
    TaskDTO findOneByIndex(@NotNull String userId, @Nullable Integer index) throws Exception;

    @NotNull
    TaskDTO removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

}
