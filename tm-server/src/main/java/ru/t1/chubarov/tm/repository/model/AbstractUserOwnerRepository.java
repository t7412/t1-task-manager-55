package ru.t1.chubarov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.model.IUserOwnerModelRepository;
import ru.t1.chubarov.tm.model.AbstractUserOwnerModel;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnerRepository<M extends AbstractUserOwnerModel> extends AbstractRepository<M> implements IUserOwnerModelRepository<M> {

//    public AbstractUserOwnerRepository(@NotNull final EntityManager entityManager) {
//        super(entityManager);
//    }

    @Override
    public void add(@Nullable final String userId, @NotNull final M model) {
        if (userId != null) {
            model.setUserId(userId);
            add(model);
        }
    }

    @Nullable
    @Override
    public abstract M findAllByUser(@Nullable final String userId);

    @Nullable
    @Override
    public abstract M findOneByIdByUser(@Nullable final String userId, @Nullable final String id);

    @Override
    public abstract void remove(@Nullable final String userId, @NotNull final M model);

    @Override
    public abstract int getSizeByUser(@Nullable final String userId);

    @Override
    public abstract void removeOneById(@Nullable final String userId, @Nullable final String id);

    @Override
    public abstract void removeAll(@Nullable final String userId);

    @Override
    public abstract Boolean existsById(@Nullable final String userId, @Nullable final String id);

}
