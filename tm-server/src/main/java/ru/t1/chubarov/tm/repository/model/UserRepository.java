package ru.t1.chubarov.tm.repository.model;

import lombok.Getter;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.chubarov.tm.api.repository.model.IUserModelRepository;
import ru.t1.chubarov.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

@Getter
@Repository
@Scope("prototype")
public final class UserRepository extends AbstractRepository<User> implements IUserModelRepository {

//    public UserRepository(@NotNull EntityManager entityManager) {
//        super(entityManager);
//    }

    @Nullable
    @Override
    public List<User> findAll() {
        return entityManager
                .createQuery("SELECT p FROM User p ", User.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public User findOneById(@NotNull String id) {
        return entityManager
                .find(User.class, id);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull String login) {
        return entityManager
                .createQuery("SELECT p FROM User p WHERE p.login = :login", User.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("login", login)
                .setFirstResult(0)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull String email) {
        return entityManager
                .createQuery("SELECT p FROM User p WHERE p.email = :email", User.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("email", email)
                .setFirstResult(0)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM User").executeUpdate();
    }

    @Override
    public void removeOneById(@Nullable String id) {
        entityManager
                .createQuery("DELETE FROM User p WHERE p.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public long getSize() {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM User p", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getSingleResult();
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull String login) {
        return entityManager
                .createQuery("SELECT COUNT(p) > 0 FROM User p WHERE p.login = :login", Boolean.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("login", login)
                .setMaxResults(1)
                .getSingleResult();
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull String email) {
        return entityManager
                .createQuery("SELECT COUNT(p) > 0 FROM User p WHERE p.email = :email", Boolean.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("email", email)
                .setMaxResults(1)
                .getSingleResult();
    }

}
