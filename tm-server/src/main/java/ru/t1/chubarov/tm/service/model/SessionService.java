package ru.t1.chubarov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.chubarov.tm.api.repository.model.ISessionModelRepository;
import ru.t1.chubarov.tm.api.service.model.ISessionService;
import ru.t1.chubarov.tm.exception.entity.ModelNotFoundException;
import ru.t1.chubarov.tm.exception.entity.SessionNotFoundException;
import ru.t1.chubarov.tm.exception.field.IdEmptyException;
import ru.t1.chubarov.tm.exception.field.UserIdEmptyException;
import ru.t1.chubarov.tm.model.Session;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Service
public class SessionService implements ISessionService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    public ISessionModelRepository getSessionRepository() {
        return context.getBean(ISessionModelRepository.class);
    }

    @Override
    public void add(@Nullable String userId, @Nullable Session model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull final ISessionModelRepository repository = getSessionRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<Session> findAll() throws Exception {
        @NotNull final ISessionModelRepository repository = getSessionRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<Session> findAll(@NotNull String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ISessionModelRepository repository = getSessionRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAllByUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Session findOneById(@NotNull String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final ISessionModelRepository repository = getSessionRepository();
        try {
            @Nullable final Session model = repository.findOneById(id);
            if (model == null) throw new SessionNotFoundException();
            return model;
        } catch (@NotNull final Exception e) {
            throw e;
        }
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        @NotNull final ISessionModelRepository repository = getSessionRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@NotNull final String userId, @Nullable Session model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull final ISessionModelRepository repository = getSessionRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeOneById(userId, model.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll(@NotNull final String userId, @Nullable final Collection<Session> models) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ISessionModelRepository repository = getSessionRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            for (@NotNull final Session session : models) {
                repository.removeOneById(userId, session.getId());
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session model = new Session();
        model.setId(id);
        @NotNull final ISessionModelRepository repository = getSessionRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeOneById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session model = new Session();
        model.setId(id);
        @NotNull final ISessionModelRepository repository = getSessionRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ISessionModelRepository repository = getSessionRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSizeByUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize() throws Exception {
        @NotNull final ISessionModelRepository repository = getSessionRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

}
