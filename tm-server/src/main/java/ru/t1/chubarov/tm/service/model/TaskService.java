package ru.t1.chubarov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.chubarov.tm.api.repository.model.ITaskModelRepository;
import ru.t1.chubarov.tm.api.repository.model.IUserModelRepository;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.api.service.model.ITaskService;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.entity.ModelNotFoundException;
import ru.t1.chubarov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.chubarov.tm.exception.entity.TaskNotFoundException;
import ru.t1.chubarov.tm.exception.field.IdEmptyException;
import ru.t1.chubarov.tm.exception.field.NameEmptyException;
import ru.t1.chubarov.tm.exception.field.UserIdEmptyException;
import ru.t1.chubarov.tm.exception.user.UserNotFoundException;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.repository.model.UserRepository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public final class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    public ITaskModelRepository getTaskRepository() {
        return context.getBean(ITaskModelRepository.class);
    }

    @NotNull
    public IUserModelRepository getUserRepository() {
        return context.getBean(IUserModelRepository.class);
    }

    public User findOneById(@Nullable final String userId) {
        @NotNull final IUserModelRepository repositoryUser = getUserRepository();
        return repositoryUser.findOneById(userId);
    }

    @Override
    public void add(@Nullable String userId, @Nullable Task model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull final ITaskModelRepository repository = getTaskRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @NotNull final String name,
            @NotNull final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        @NotNull Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        @NotNull final User user = Optional.of(findOneById(userId)).orElseThrow(UserNotFoundException::new);
        task.setUser(user);
        @NotNull final ITaskModelRepository repository = getTaskRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @NotNull final Status status) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull Task task = findOneById(userId, id);
        task.setStatus(status.toString());
        @NotNull final User user = Optional.of(findOneById(userId)).orElseThrow(UserNotFoundException::new);
        task.setUser(user);
        @NotNull final ITaskModelRepository repository = getTaskRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        @NotNull final ITaskModelRepository repository = getTaskRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ITaskModelRepository repository = getTaskRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAllByUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll() throws Exception {
        @NotNull final ITaskModelRepository repository = getTaskRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Task findOneById(@NotNull final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final ITaskModelRepository repository = getTaskRepository();
        try {
            @Nullable final Task model = repository.findOneById(id);
            if (model == null) throw new TaskNotFoundException();
            return model;
        } catch (@NotNull final Exception e) {
            throw e;
        }
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        @NotNull final ITaskModelRepository repository = getTaskRepository();
        try {
            return repository.findAllByProjectId(userId, projectId);
        } catch (@NotNull final Exception e) {
            throw e;
        }
    }

    @Override
    public void remove(@NotNull final String userId, @Nullable final Task model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull final ITaskModelRepository repository = getTaskRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ITaskModelRepository repository = getTaskRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeAll(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Task model = new Task();
        @NotNull final ITaskModelRepository repository = getTaskRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeOneById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize() {
        @NotNull final ITaskModelRepository repository = getTaskRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ITaskModelRepository repository = getTaskRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSizeByUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void add(@NotNull final Collection<Task> models) throws Exception {
        if (models == null) throw new ProjectNotFoundException();
        @NotNull final ITaskModelRepository repository = getTaskRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            for (@NotNull final Task task : models) {
                repository.add(task);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void set(@NotNull final Collection<Task> models) throws Exception {
        if (models == null) throw new ProjectNotFoundException();
        @NotNull final ITaskModelRepository repository = getTaskRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
            for (@NotNull final Task task : models) {
                repository.add(task);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final ITaskModelRepository repository = getTaskRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
