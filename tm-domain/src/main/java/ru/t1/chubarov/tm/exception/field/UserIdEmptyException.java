package ru.t1.chubarov.tm.exception.field;

public final class UserIdEmptyException extends AbstractFieldException {

    public UserIdEmptyException() {
        super("Error. UserId is empty.");
    }

}
