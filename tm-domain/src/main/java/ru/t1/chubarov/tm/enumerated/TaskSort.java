package ru.t1.chubarov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.comparator.CreatedComparator;
import ru.t1.chubarov.tm.comparator.NameComparator;
import ru.t1.chubarov.tm.comparator.StatusComparator;
import ru.t1.chubarov.tm.dto.model.TaskDTO;

import java.util.Comparator;

public enum TaskSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare);

    @NotNull
    public static TaskSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return BY_NAME;
        for (@NotNull final TaskSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return BY_NAME;
    }

    @Nullable
    private final String name;
    @Nullable
    private final Comparator<TaskDTO> comparator;

    TaskSort(@Nullable final String name, @Nullable final Comparator<TaskDTO> comparator) {
        this.name = name;
        this.comparator = comparator;
    }

    @Nullable
    public String getName() {
        return name;
    }

    @Nullable
    public Comparator<TaskDTO> getComparator() {
        return comparator;
    }

}
