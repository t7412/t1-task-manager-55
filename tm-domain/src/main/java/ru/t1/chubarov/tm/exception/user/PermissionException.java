package ru.t1.chubarov.tm.exception.user;

import ru.t1.chubarov.tm.exception.field.AbstractFieldException;

public final class PermissionException extends AbstractFieldException {

    public PermissionException() {
        super("Error. Permission is incorrect.");
    }

}
