package ru.t1.chubarov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.dto.request.DataBinarySaveRequest;
import ru.t1.chubarov.tm.enumerated.Role;

@Component
public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-save-bin";
    @NotNull
    private final String DESCRIPTION = "Save data to binary file.";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE BINARY]");
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest(getToken());
        domainEndpoint.saveDataBinary(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
