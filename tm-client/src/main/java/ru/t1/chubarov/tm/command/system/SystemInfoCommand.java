package ru.t1.chubarov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.util.FormatUtil;

@Component
public final class SystemInfoCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory    : " + FormatUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        @Nullable final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        @Nullable final String maxMemoryValue = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory : " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory   : " + FormatUtil.formatBytes(totalMemory));
        final long usageMemory = totalMemory - freeMemory;
        System.out.println("Usage memory   : " + FormatUtil.formatBytes(usageMemory));
    }

    @NotNull
    @Override
    public String getName() {
        return "info";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show system info.";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-i";
    }

}
