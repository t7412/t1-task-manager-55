package ru.t1.chubarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.dto.request.TaskListByProjectIdRequest;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.dto.model.TaskDTO;
import ru.t1.chubarov.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK BY PROJECT ID]");
        System.out.println("[ENTER PROJECT ID:]");
        @NotNull final String projectid = TerminalUtil.nextLine();

        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(getToken());
        request.setProjectId(projectid);
        @NotNull final List<TaskDTO> tasks = taskEndpoint.listTaskByProjectId(request).getTasks();
        if (tasks == null) System.out.println("No tasks...");
            else renderTask(tasks);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-project-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display tasks related to the project.";
    }

}
