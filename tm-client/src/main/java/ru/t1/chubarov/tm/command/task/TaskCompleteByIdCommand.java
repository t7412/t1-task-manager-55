package ru.t1.chubarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.dto.request.TaskChangeStatusByIdRequest;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.util.TerminalUtil;

@Component
public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE TASK BY ID");
        System.out.println("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final Status status = Status.toStatus("COMPLETED");
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken());
        request.setId(id);
        request.setStatus(status);
        taskEndpoint.changeTaskStatusById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-complete-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update task status to [Completed] by id.";
    }

}
