package ru.t1.chubarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.dto.request.TaskRemoveByIdRequest;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE TASK BY ID");
        System.out.println("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(getToken());
        request.setId(id);
        taskEndpoint.removeTaskById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-remove-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove task by Id.";
    }

}
