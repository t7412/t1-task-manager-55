package ru.t1.chubarov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.chubarov.tm.api.service.ICommandService;
import ru.t1.chubarov.tm.command.AbstractCommand;
import ru.t1.chubarov.tm.enumerated.Role;

@Component
public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    @Autowired
    public ICommandService commandService;

    @NotNull
    public ISystemEndpoint systemEndpoint;

    @Nullable
    public String getArgument() {
        return null;
    }

    @Nullable
    public Role[] getRoles() {
        return null;
    }

}
