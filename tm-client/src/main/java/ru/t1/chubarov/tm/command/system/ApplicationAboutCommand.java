package ru.t1.chubarov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.dto.request.ApplicationAboutRequest;

@Component
public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        @NotNull final ApplicationAboutRequest request = new ApplicationAboutRequest();
        System.out.println("name: " + systemEndpoint.getAbout(request).getName());
        System.out.println("e-mail: " + systemEndpoint.getAbout(request).getEmail());
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show about program.";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }

}
