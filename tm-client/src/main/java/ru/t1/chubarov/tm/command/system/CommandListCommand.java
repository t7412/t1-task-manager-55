package ru.t1.chubarov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class CommandListCommand extends AbstractSystemCommand {
    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @Nullable final Collection<AbstractCommand> commands = commandService.getTerminalCommands();
        for (@Nullable final AbstractCommand command : commands) {
            if (command == null) continue;
            @Nullable final String name = getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "commands";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show command list.";
    }
}
