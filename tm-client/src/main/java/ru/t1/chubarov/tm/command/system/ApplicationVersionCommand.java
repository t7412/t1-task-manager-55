package ru.t1.chubarov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.dto.request.ApplicationVersionRequest;

@Component
public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        @NotNull final ApplicationVersionRequest request = new ApplicationVersionRequest();
        System.out.println(systemEndpoint.getVersion(request).getVersion());
    }

    @NotNull
    @Override
    public String getName() {
        return "version";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show program version.";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-v";
    }

}
