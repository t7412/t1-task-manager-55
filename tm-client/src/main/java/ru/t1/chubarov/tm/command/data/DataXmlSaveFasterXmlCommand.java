package ru.t1.chubarov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.dto.request.DataXmlSaveFasterXmlRequest;
import ru.t1.chubarov.tm.enumerated.Role;

@Component
public final class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-save-xml-faster";
    @NotNull
    private final String DESCRIPTION = "Save data in xml file.";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        @NotNull final DataXmlSaveFasterXmlRequest request = new DataXmlSaveFasterXmlRequest(getToken());
        domainEndpoint.saveDataXmlFasterXml(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
