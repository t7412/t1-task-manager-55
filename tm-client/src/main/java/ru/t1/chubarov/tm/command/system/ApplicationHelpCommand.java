package ru.t1.chubarov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = commandService.getTerminalCommands();
        for (@Nullable final AbstractCommand command : commands) {
            System.out.println(command.toString());
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show help program.";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-h";
    }

}
